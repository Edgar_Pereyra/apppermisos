import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { MatSelectModule } from '@angular/material/select';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatInputModule } from '@angular/material/input'
import { MatIconModule} from '@angular/material/icon';
import { ErrorComponent } from './error/error.component';
import { FlashMessagesModule } from 'angular2-flash-messages';
import { AngularMaterialModule } from './Material/material.module';
import { PermisosComponent } from './permisos/permisos.component';
import { TipopermisosComponent } from './tipopermisos/tipopermisos.component';
import { Ng2SmartTableModule } from 'ng2-smart-table';


@NgModule({
  declarations: [
    AppComponent,
    ErrorComponent,
    PermisosComponent,
    TipopermisosComponent,
    TipopermisosComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    MatSelectModule,
    MatCheckboxModule,
    MatIconModule,
    MatInputModule,
    FlashMessagesModule.forRoot(),
    ReactiveFormsModule,
    AngularMaterialModule,
    Ng2SmartTableModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
