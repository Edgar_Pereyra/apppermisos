import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ErrorComponent } from './error/error.component';
import { PermisosComponent } from './permisos/permisos.component';
import { TipopermisosComponent } from './tipopermisos/tipopermisos.component';

const routes: Routes = [
  {
    path: 'permisos',
    component: PermisosComponent
  },
  {
    path: 'tipopermisos',
    component: TipopermisosComponent
  },
  {
    path: '',
    component: PermisosComponent
  },
  {
    path: '**',
    component: ErrorComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
