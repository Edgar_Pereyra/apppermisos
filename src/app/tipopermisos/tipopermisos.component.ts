import { Component, OnInit } from '@angular/core';
import { FlashMessagesService } from 'angular2-flash-messages';
import { TipoPermisoModel } from '../Models/tipoPermisoModel.model';
import { TipoPermisoService } from '../Servicios/tipoPermisoService.service';

@Component({
  selector: 'app-tipopermisos',
  templateUrl: './tipopermisos.component.html',
  styleUrls: ['./tipopermisos.component.scss'],
})
export class TipopermisosComponent implements OnInit {
  tipoPermisos: TipoPermisoModel[];

  settings = {
    hideSubHeader: true,
    delete: {
      deleteButtonContent: '<i class="nb-trash">Eliminar</i>',
      confirmDelete: true,
    },
    actions: {
      columnTitle: "",
      add: false,
      edit: false,
      delete: true,
    },
    pager: {
      display: true,
      perPage: 10,
    },
    defaultStyle: false,
    noDataMessage: 'Sin datos',
    columns: {
      id: {
        title: 'Id',
      },
      vchDescripcion: {
        title: 'Tipo de permiso',
      },
    },
  };

  constructor(private tipoPermisoService: TipoPermisoService,
    private flashMessage: FlashMessagesService) {}

  ngOnInit(): void {
    this.tipoPermisoService.mostrarTipoPermisos().subscribe(
      (res: TipoPermisoModel[]) => {
        this.tipoPermisos = res;
        console.log(res);
      },
      (error) => {
        const errorMessage = <any>error;
        console.error(errorMessage);
      }
    );
  }

  onDeleteConfirm(event: any) {
    console.log("Delete Event In Console")
    console.log(event.data);
    if (window.confirm('¿Eliminar registro?')) {
      this.tipoPermisoService.eliminarTipoPermiso(event.data.id).subscribe(
        (res) => {
          if(res)
          {
            this.flashMessage.show('Datos guardados con éxito', {
              timeout: 4000,
            });
            event.confirm.resolve();
          }
          else
          {
            this.flashMessage.show('Ocurrio un error al guardar la información.', {
              timeout: 4000,
            });
            event.confirm.reject();
          }
        },
        (error) => {
          const errorMessage = <any>error;
          console.error();
          this.flashMessage.show('Ocurrio un error al guardar la información.' + errorMessage, {
            timeout: 4000,
          });
          event.confirm.reject();
        }
      );
    } else {
      event.confirm.reject();
    }
  }
}
