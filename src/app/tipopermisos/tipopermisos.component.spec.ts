import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TipopermisosComponent } from './tipopermisos.component';

describe('TipopermisosComponent', () => {
  let component: TipopermisosComponent;
  let fixture: ComponentFixture<TipopermisosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TipopermisosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TipopermisosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
