import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { PermisoModel } from '../Models/Permiso.model';

@Injectable({
  providedIn: 'root'
})

export class permisoService {

  readonly url = environment.apiUrl;

  constructor(private http:HttpClient) { }

  public mostrarPermisos(): Observable<any>{
    return this.http.get<PermisoModel>(`${this.url}/api/Permisos/getPermisos`);
  }

  public actualizarPermiso(_permiso: PermisoModel) {
    return this.http.put<any>(`${this.url}/api/Permisos/UpdatePermiso`, _permiso);
  }
  
  public agregarPermiso(_permiso: PermisoModel) {
    return this.http.post<any>(`${this.url}/api/Permisos/AddPermiso`, _permiso);
  }
  
}
