import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { TipoPermisoModel } from '../Models/tipoPermisoModel.model';

@Injectable({
  providedIn: 'root'
})

export class TipoPermisoService {

  readonly url = environment.apiUrl;

  constructor(private http:HttpClient) { }

  public mostrarTipoPermisos(): Observable<any>{
    return this.http.get<TipoPermisoModel>(`${this.url}/api/TipoPermisos/GetTipoPermisos`);
  }

  public eliminarTipoPermiso(id: number) {
    return this.http.delete<any>(`${this.url}/api/TipoPermisos/DeletePermiso=` + id);
  }

}