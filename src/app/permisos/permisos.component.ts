import { Component, OnDestroy, OnInit } from '@angular/core';
import { FlashMessagesService } from 'angular2-flash-messages';
import { TipoPermisoModel } from '../Models/tipoPermisoModel.model';
import { TipoPermisoService } from '../Servicios/tipoPermisoService.service';
import { PermisoModel } from '../Models/Permiso.model';
import { permisoService } from '../Servicios/permisoService.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-permisos',
  templateUrl: './permisos.component.html',
  styleUrls: ['./permisos.component.scss'],
  providers: [permisoService],
})
export class PermisosComponent implements OnInit, OnDestroy {
  permisoModel: PermisoModel = new PermisoModel();
  permisos: PermisoModel[] = [];
  tipoPermisos: TipoPermisoModel[];
  idTipoPermiso: number;
  idPersona: number = 0;
  formAdd: FormGroup;
  formUpdate: FormGroup;

  constructor(
    private permisoService: permisoService,
    private tipoPermisoService: TipoPermisoService,
    private flashMessage: FlashMessagesService,
    public fb: FormBuilder
  ) {}

  ngOnDestroy(): void {
    this.formAdd.reset();
    this.formUpdate.reset();
  }

  ngOnInit(): void {
    this.inicio();
  }

  inicio() {
    this.permisoModel = new PermisoModel();
    this.permisos = [];
    this.tipoPermisos = [];
    this.idPersona = 0;
    this.idTipoPermiso = 0;
    this.reactiveForm();

    this.permisoService.mostrarPermisos().subscribe(
      (res: PermisoModel[]) => {
        this.permisos = res;

        this.tipoPermisoService.mostrarTipoPermisos().subscribe(
          (res: TipoPermisoModel[]) => {
            this.tipoPermisos = res;
          },
          (error) => {
            const errorMessage = <any>error;
            console.error(errorMessage);
          }
        );
      },
      (error) => {
        const errorMessage = <any>error;
        console.error(errorMessage);
      }
    );
  }

  reactiveForm() {
    this.formAdd = this.fb.group({
      addNombre: ['', [Validators.required]],
      addApellido: ['', [Validators.required]],
      addFecha: ['', [Validators.required]],
      addTipoPermiso: [0, [Validators.required]],
    });

    this.formUpdate = this.fb.group({
      UpdNombre: ['', [Validators.required]],
      UpdApellido: ['', [Validators.required]],
      UpdFecha: ['', [Validators.required]],
      UpdTipoPermiso: [0, [Validators.required]],
    });
  }

  onAgregarPermiso(): void {
    this.permisoModel.iTipoPermiso = this.idTipoPermiso;
    this.permisoService.agregarPermiso(this.permisoModel).subscribe(
      (res) => {
        this.flashMessage.show('Datos guardados con éxito', {
          timeout: 4000,
        });
        this.ngOnDestroy();
      },
      (error) => {
        const errorMessage = <any>error;
        this.flashMessage.show(
          'Ocurrio un error al guardar la información: ' + errorMessage,
          { timeout: 4000 }
        );
        console.error(errorMessage);
      }
    );
  }

  onEditarPermiso() {
    this.permisoModel.iTipoPermiso = this.idTipoPermiso;
    this.permisoModel.id = this.idPersona;

    this.permisoService.actualizarPermiso(this.permisoModel).subscribe(
      (res) => {
        this.flashMessage.show('Información actualizada con éxito', {
          timeout: 4000,
        });
        this.idPersona = 0;
        this.ngOnDestroy();
      },
      (error) => {
        const errorMessage = <any>error;
        this.flashMessage.show(
          'Ocurrio un error al guardar la información: ' + errorMessage,
          { timeout: 4000 }
        );
        console.error(errorMessage);
      }
    );
  }

  onCancelarEdicion() {
    this.permisoModel = new PermisoModel();
    this.idPersona = 0;
  }
}
