export class PermisoModel {
  public id: number;
  public vchNombreEmpleado: string;
  public vchApellidosEmpleado: string;
  public iTipoPermiso: number;
  public dtFechaPermiso: string;
}
